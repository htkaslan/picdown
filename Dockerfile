FROM ruby:2.7

WORKDIR /app
COPY . ./

RUN bundle config --global silence_root_warning true
RUN bundle config --global set path vendor/bundle
RUN bundle install
RUN bundle exec rake install
RUN gem install pkg/picdown-*.gem

ENTRYPOINT ["picdown"]
