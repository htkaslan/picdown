Picdown ![pipeline](https://gitlab.com/htkaslan/picdown/badges/master/pipeline.svg) [![Codacy Badge](https://app.codacy.com/project/badge/Grade/349123cc896d48cb97f54f9393465f0c)](https://www.codacy.com/gl/htkaslan/picdown/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=htkaslan/picdown&amp;utm_campaign=Badge_Grade)
=======

Picdown is a command line tool that downloads the given images from a source file.
It also allows you to make parallel requests to download images. Picdown provides
fancy outputs such as progress bar for each request to inform the user.

[![asciicast](https://asciinema.org/a/ljSjsRVrkroGLHpPBFzltIIDx.svg)](https://asciinema.org/a/ljSjsRVrkroGLHpPBFzltIIDx)

Installation
------------

Picdown is dockerised, which means you can experiment it by building the image
from Dockerfile. Please run the below command to do that.

Note that you need to have a proper docker environment first.

```sh
docker build . -t picdown
```

If you want to build it manually, please follow the following instructions.

```sh
bundle config --global silence_root_warning true
bundle config --global set path vendor/bundle
bundle install
bundle exec rake install
gem install pkg/picdown-*.gem
```

Picdown is also published on [Rubygems](https://rubygems.org). You can install it by skipping the manual installation steps.

```sh
gem install picdown
```

Usage
-----

Please have a look at the help text of `picdown` for more information.

```
➜ picdown -h
Commands:
  picdown download FILE            # Download files from the specified source file.
  picdown validate PATH            # Validate source file
  picdown version                  # Print version
```

### `validate`

It validates the given source file if it contains valid URLs.

```sh
picdown validate SOURCE
```

### `download`

It downloads the images given by the source file. It needs a valid file path.

```sh
picdown download SOURCE
```

To download the images parallel:

```sh
picdown download SOURCE --parallel=4
```

You are also able to configure the download parameters. Please check the below
help text for additional parameters.

```sh
➜ picdown download -h
...

Options:
  --destination=VALUE               # Destination where files to be downloaded., default: "."
  --parallel=VALUE                  # Number of parallel download processes., default: 1
  --open-timeout=VALUE              # Open timeout for connections. (secs), default: 15
  --read-timeout=VALUE              # Read timeout for connections. (secs), default: 15
  --max-size=VALUE                  # Max file size. (bytes), default: 5242880
  --max-redirects=VALUE             # Max redirections., default: 3

...
```

Demo
----

Picdown includes a demo file, which is located in `assets`, that you can use.

```sh
picdown download assets/test.txt
```

or

```sh
docker run -t picdown download assets/test.txt
```

License
-------

Not licensed yet.
