# frozen_string_literal: true

require 'dry/cli'

require 'picdown/error'
require 'picdown/support'
require 'picdown/config'
require 'picdown/source'
require 'picdown/source_file'
require 'picdown/multithread'
require 'picdown/download'

module Picdown
  module CLI
    extend Dry::CLI::Registry

    Dir[File.join(__dir__, 'cli/*.rb')].sort.each { |command| require command }

    register 'download', Download
    register 'validate', Validate
    register 'version',  Version, aliases: ['v', '-v', '--version']
  end
end
