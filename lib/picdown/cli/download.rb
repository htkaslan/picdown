# frozen_string_literal: true

module Picdown
  module CLI
    class Download < Dry::CLI::Command
      desc 'Download files from the specified source file.'

      option :destination,   type: :string, default: '.',             desc: 'Destination where files to be downloaded.'
      option :parallel,      type: :string, default: 1,               desc: 'Number of parallel download processes.'
      option :open_timeout,  type: :string, default: 15,              desc: 'Open timeout for connections. (secs)'
      option :read_timeout,  type: :string, default: 15,              desc: 'Read timeout for connections. (secs)'
      option :max_size,      type: :string, default: 5 * 1024 * 1024, desc: 'Max file size. (bytes)'
      option :max_redirects, type: :string, default: 3,               desc: 'Max redirections.'

      argument :file, type: :string, required: true, desc: 'File to be used as a URL source.'

      example [
        '/path/to/file                               # Download files normally.',
        '/path/to/file --parallel=2                  # Perform parallel downloads.',
        '/path/to/file --destination=/path/to/target # Download to a specific destination.'
      ]

      def call(file:, **options)
        Picdown::Download.from_file(file, **options)
      end
    end
  end
end
