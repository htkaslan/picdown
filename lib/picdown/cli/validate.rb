# frozen_string_literal: true

module Picdown
  module CLI
    class Validate < Dry::CLI::Command
      desc 'Validate source file'

      argument :path, type: :string, required: true, desc: 'File path to be validated.'

      def call(path:, **)
        sources = Picdown::SourceFile.load(path)
        print_results(sources)
      end

      protected

      def print_results(sources)
        return warn 'The source file is valid!' if sources.nondownloadable.empty?

        warn 'The source file contains invalid sources.'
        sources.nondownloadable.each do |src|
          warn ''
          warn "on #{sources.path} line #{src.line}:"
          warn "  #{src.url}"
        end
      end
    end
  end
end
