# frozen_string_literal: true

require 'picdown'

module Picdown
  module CLI
    class Version < Dry::CLI::Command
      desc 'Print version'

      def call(*)
        puts Picdown::VERSION
      end
    end
  end
end
