# frozen_string_literal: true

module Picdown
  Config = Struct.new(
    :destination,
    :parallel,
    :open_timeout,
    :read_timeout,
    :max_size,
    :max_redirects,
    keyword_init: true
  ) do
    def initialize(**)
      super
      self.parallel      = parallel.to_i
      self.open_timeout  = open_timeout.to_i
      self.read_timeout  = read_timeout.to_i
      self.max_size      = max_size.to_i
      self.max_redirects = max_redirects.to_i
    end
  end
end
