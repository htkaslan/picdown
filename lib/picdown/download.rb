# frozen_string_literal: true

require 'down'
require 'tty-progressbar'

module Picdown
  class Download
    using Support::Refinements

    attr_reader :source, :config, :bars

    def initialize(file, **opts)
      @source = SourceFile.load(file)
      @config = Config.new(opts)
      @bars   = TTY::ProgressBar::Multi.new
    end

    def call
      config.parallel <= 1 ? perform_sequential : perform_parallel
    end

    def perform_sequential
      source.downloadable.each { |src| download(src) }
    end

    def perform_parallel
      threads = MultiThread.new(config.parallel)
      source.downloadable.each { |src| threads.add { download(src) } }
      threads.invoke
    end

    class << self
      def from_file(file, **opts)
        new(file, **opts).call
      end
    end

    private

    def download(src) # rubocop:disable Metrics/AbcSize,Metrics/MethodLength
      bar = bars.register "#{src.url.shorten} [:bar] :percent", total: 100, width: 30
      total_size = 0

      tempfile = Down.download(
        src.url,
        **config.to_h.except(:destination, :parallel),
        content_length_proc: ->(content_length) { total_size += content_length },
        progress_proc: lambda do |progress|
          break if progress == total_size

          bar.current = (progress.to_f / total_size) * 100
        end
      )

      # Safely interrupt the download if the file is not an image.
      content_type = tempfile.content_type
      raise MimeTypeError, 'The file is not an image.' unless Support::MIME_TYPES.include?(content_type)

      # Move the tempfile to the destination.
      name = tempfile.original_filename || File.basename(tempfile.path)
      dest = File.join(config.destination, name)
      FileUtils.mv(tempfile.path, dest)
    rescue Down::Error => e
      raise DownloadError, "FAILED: #{src.url}: #{e.message}"
    ensure
      bar.finish
      tempfile&.close
    end
  end
end
