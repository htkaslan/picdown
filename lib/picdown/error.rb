# frozen_string_literal: true

module Picdown
  Error         = Class.new(StandardError)
  MimeTypeError = Class.new(Error)
  DownloadError = Class.new(Error)
end
