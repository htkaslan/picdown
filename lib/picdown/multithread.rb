# frozen_string_literal: true

module Picdown
  class MultiThread
    Thread.report_on_exception = false

    attr_reader :pool, :threads, :queue

    def initialize(pool)
      @pool    = pool
      @threads = []
      @queue   = Queue.new
    end

    def add(*args, &block)
      queue << [block, args]
    end

    def invoke
      pool.times { threads << spawn_thread }
      trap('INT') { threads.each(&:kill) }
      threads.each(&:join)
    end

    private

    def spawn_thread
      Thread.new do
        Thread.handle_interrupt(Exception => :never) do
          Thread.handle_interrupt(Exception => :immediate) do
            until queue.empty?
              block, args = queue.pop
              block&.call(*args)
            end
          end
        end
      end
    end
  end
end
