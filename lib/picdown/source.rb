# frozen_string_literal: true

module Picdown
  Source = Struct.new(:line, :url) do
    def valid?
      url.match?(URI::DEFAULT_PARSER.make_regexp)
    end
  end
end
