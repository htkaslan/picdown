# frozen_string_literal: true

module Picdown
  class SourceFile
    include Enumerable

    attr_reader :path, :sources

    def initialize(path)
      @path    = path
      @sources = []
    end

    def <<(src)
      sources << src
    end

    def each(&block)
      sources.each(&block)
    end

    def load
      File.readlines(path).each_with_index do |line, i|
        sources << Source.new(i + 1, line.chomp)
      end

      self
    end

    def downloadable
      select(&:valid?)
    end

    def nondownloadable
      sources - downloadable
    end

    class << self
      def load(path)
        new(path).load
      end
    end
  end
end
