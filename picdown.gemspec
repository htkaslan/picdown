# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)
require 'picdown/version'

Gem::Specification.new do |spec|
  spec.name                  = 'picdown'
  spec.author                = 'Hüseyin Tekinaslan'
  spec.email                 = 'htkaslan@gmail.com'
  spec.summary               = 'Download the given images from a source file.'
  spec.description           = spec.summary
  spec.homepage              = 'https://github.com/huseyin/picdown'
  spec.files                 = Dir['README.md', 'Rakefile', 'picdown.gemspec', 'lib/**/*']
  spec.require_paths         = ['lib']
  spec.bindir                = 'bin'
  spec.executables           = ['picdown']
  spec.required_ruby_version = Gem::Requirement.new('>= 2.7')
  spec.version               = Picdown::VERSION

  spec.add_dependency 'down', '~> 5.0'
  spec.add_dependency 'dry-cli'
  spec.add_dependency 'tty-progressbar'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'minitest-focus'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-minitest'
  spec.add_development_dependency 'rubocop-performance'
  spec.add_development_dependency 'rubocop-rake'
  spec.add_development_dependency 'vcr'
  spec.add_development_dependency 'webmock'
end
