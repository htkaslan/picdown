# frozen_string_literal: true

require 'test_helper'
require 'picdown/config'

class ConfigTest < Minitest::Test
  def setup
    @config = Picdown::Config.new(
      destination: 'DEST',
      parallel: 5,
      open_timeout: 10,
      read_timeout: 10,
      max_size: 1 * 1024 * 1024, # 1 MB
      max_redirects: 1
    )
  end

  def test_that_config_has_correct_destination
    assert_equal 'DEST', @config.destination
  end

  def test_that_config_has_correct_parallel
    assert_equal 5, @config.parallel
  end

  def test_that_config_has_correct_open_timeout
    assert_equal 10, @config.open_timeout
  end

  def test_that_config_has_correct_read_timeout
    assert_equal 10, @config.read_timeout
  end

  def test_that_config_has_correct_max_size
    assert_equal 1 * 1024 * 1024, @config.max_size
  end

  def test_that_config_has_correct_max_redirects
    assert_equal 1, @config.max_redirects
  end
end
