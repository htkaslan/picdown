# frozen_string_literal: true

require 'test_helper'
require 'picdown/support'
require 'picdown/multithread'
require 'picdown/error'
require 'picdown/download'

class DownloadTest < Minitest::Test
  MAX_SIZE = 5 * 1024 * 1024

  def setup
    @tempfile = Tempfile.new
    @tempfile.write 'http://via.placeholder.com/640x360'
    @tempfile.read
  end

  def test_sequential_download
    VCR.use_cassette('image_640_x_360') do
      assert Picdown::Download.from_file(
        @tempfile.path, parallel: 1, max_size: MAX_SIZE, destination: File.dirname(@tempfile.path)
      )
    end
  ensure
    @tempfile.unlink
  end

  def test_parallel_download
    VCR.use_cassette('image_640_x_360') do
      assert Picdown::Download.from_file(
        @tempfile.path, parallel: 2, max_size: MAX_SIZE, destination: File.dirname(@tempfile.path)
      )
    end
  ensure
    @tempfile.unlink
  end
end
