# frozen_string_literal: true

require 'test_helper'
require 'picdown/multithread'

class MultiThreadTest < Minitest::Test
  def setup
    @threads = Picdown::MultiThread.new(2)
  end

  def test_to_add_thread
    assert(@threads.add { 'I am a thread' })
  end

  def test_to_invoke_all_threads
    assert @threads.invoke
  end

  def test_that_sigint_works
    fork { @threads.invoke }
    Process.kill('INT', Process.pid)
    assert Process.wait
  end
end
