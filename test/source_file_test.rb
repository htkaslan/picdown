# frozen_string_literal: true

require 'test_helper'
require 'picdown/source_file'

class SourceFleTest < Minitest::Test
  def setup
    @tempfile = Tempfile.new
    @tempfile.write "https://example.com\nINVALID"
    @tempfile.read
    @source_file = Picdown::SourceFile.load(@tempfile.path)
  end

  def test_that_source_file_has_correct_downloadable_sources
    source = @source_file.downloadable.first
    assert_equal 1, source.line
    assert_equal 'https://example.com', source.url
  ensure
    @tempfile.unlink
  end

  def test_that_source_file_has_correct_nondownloadable_sources
    source = @source_file.nondownloadable.first
    assert_equal 2, source.line
    assert_equal 'INVALID', source.url
  ensure
    @tempfile.unlink
  end
end
