# frozen_string_literal: true

require 'test_helper'
require 'picdown/source'

class SourceTest < Minitest::Test
  def setup
    @source = Picdown::Source.new(1, 'https://example.com')
  end

  def test_that_source_has_correct_attributes
    assert_equal 1, @source.line
    assert_equal 'https://example.com', @source.url
  end

  def test_that_validity_of_source
    assert @source.valid?

    @source.url = 'INVALID'
    refute @source.valid?
  end
end
