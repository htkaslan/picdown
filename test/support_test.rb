# frozen_string_literal: true

require 'test_helper'
require 'picdown/support'

class SupportTest < Minitest::Test
  using Picdown::Support::Refinements

  def test_that_except_works
    hash = { x: 'x', y: 'y' }
    assert_equal({ x: 'x' }, hash.except(:y))
  end

  def test_that_shorten_works
    assert_equal 'lor...met', 'lorem ipsum dolor sit amet'.shorten(limit: 3)
  end
end
